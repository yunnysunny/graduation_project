/**
* DES/N-DES加密程序
* @version 1.1b
* @author 高阳 yunnysunny@sohu.com QQ:243853184
* @copyright 任何人都可以引用或修改本程序或者本程序中的函数，但必须保留以上信息 
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <conio.h>
#define USEFILE 0
typedef struct KeyElement {
	struct KeyElement *next;
	char data[64];
}keyElement;
char intKey[8] = {
	1,35,69,103,137,85,102,119
};//原始的key，注意每个key只有7位，所以每个key不能超过127 
int step = 0;
char pc1[] = {			/* permuted choice table (key)置换选择1  */	
	57, 49, 41, 33, 25, 17,  9,
	 1, 58, 50, 42, 34, 26, 18,
	10,  2, 59, 51, 43, 35, 27,
	19, 11,  3, 60, 52, 44, 36,

	63, 55, 47, 39, 31, 23, 15,
	 7, 62, 54, 46, 38, 30, 22,
	14,  6, 61, 53, 45, 37, 29,
	21, 13,  5, 28, 20, 12,  4	
};

char pc2[] = {	       /* permuted choice key (table)置换选择2  */
	14, 17, 11, 24,  1,  5,
	 3, 28, 15,  6, 21, 10,
	23, 19, 12,  4, 26,  8,
	16,  7, 27, 20, 13,  2,
	41, 52, 31, 37, 47, 55,
	30, 40, 51, 45, 33, 48,
	44, 49, 39, 56, 34, 53,
	46, 42, 50, 36, 29, 32	
};

char ip[] = /* initial permutation P 初始置换IP	*/
{	
	58, 50, 42, 34, 26, 18, 10,  2,
	60, 52, 44, 36, 28, 20, 12,  4,
	62, 54, 46, 38, 30, 22, 14,  6,
	64, 56, 48, 40, 32, 24, 16,  8,
	57, 49, 41, 33, 25, 17,  9,  1,
	59, 51, 43, 35, 27, 19, 11,  3,
	61, 53, 45, 37, 29, 21, 13,  5,
	63, 55, 47, 39, 31, 23, 15,  7	
};

char pe[] = {
	32, 1, 2, 3, 4, 5,
	4,  5, 6, 7, 8, 9,
	8,  9, 10,11,12,13,
	12,13, 14,15,16,17,
	16,17, 18,19,20,21,
	20,21, 22,23,24,25,
	24,25, 26,27,28,29,
	28,29, 30,31,32,1
};

char si[8][64] = {			  /* 48->32 bit compression tables*/
					/* S[1]			 */
	14,  4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7,
	 0, 15,  7,  4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5,  3,  8,
	 4,  1, 14,  8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10,  5,  0,
	15, 12,  8,  2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0,  6, 13,
					/* S[2]			 */
	15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10,
	 3, 13,  4,  7, 15,  2,  8, 14, 12,  0,  1, 10,  6,  9, 11,  5,
	 0, 14,  7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15,
	13,  8, 10,  1,  3, 15,  4,  2, 11,  6,  7, 12,  0,  5, 14,  9,
					/* S[3]			 */
	10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8,
	13,  7,  0,  9,  3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1,
	13,  6,  4,  9,  8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7,
	 1, 10, 13,  0,  6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12,
					/* S[4]			 */
	 7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15,
	13,  8, 11,  5,  6, 15,  0,  3,  4,  7,  2, 12,  1, 10, 14,  9,
	10,  6,  9,  0, 12, 11,  7, 13, 15,  1,  3, 14,  5,  2,  8,  4,
	 3, 15,  0,  6, 10,  1, 13,  8,  9,  4,  5, 11, 12,  7,  2, 14,
					/* S[5]			 */
	 2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13,  0, 14,  9,
	14, 11,  2, 12,  4,  7, 13,  1,  5,  0, 15, 10,  3,  9,  8,  6,
	 4,  2,  1, 11, 10, 13,  7,  8, 15,  9, 12,  5,  6,  3,  0, 14,
	11,  8, 12,  7,  1, 14,  2, 13,  6, 15,  0,  9, 10,  4,  5,  3,
					/* S[6]			 */
	12,  1, 10, 15,  9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11,
	10, 15,  4,  2,  7, 12,  9,  5,  6,  1, 13, 14,  0, 11,  3,  8,
	 9, 14, 15,  5,  2,  8, 12,  3,  7,  0,  4, 10,  1, 13, 11,  6,
	 4,  3,  2, 12,  9,  5, 15, 10, 11, 14,  1,  7,  6,  0,  8, 13,
					/* S[7]			 */
	 4, 11,  2, 14, 15,  0,  8, 13,  3, 12,  9,  7,  5, 10,  6,  1,
	13,  0, 11,  7,  4,  9,  1, 10, 14,  3,  5, 12,  2, 15,  8,  6,
	 1,  4, 11, 13, 12,  3,  7, 14, 10, 15,  6,  8,  0,  5,  9,  2,
	 6, 11, 13,  8,  1,  4, 10,  7,  9,  5,  0, 15, 14,  2,  3, 12,
					/* S[8]			 */
	13,  2,  8,  4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7,
	 1, 15, 13,  8, 10,  3,  7,  4, 12,  5,  6, 11,  0, 14,  9,  2,
	 7, 11,  4,  1,  9, 12, 14,  2,  0,  6, 10, 13, 15,  3,  5,  8,
	 2,  1, 14,  7,  4, 10,  8, 13, 15, 12,  9,  0,  3,  5,  6, 11	
 };
 
 char p32i[] = {				/* 32-bit permutation function置换矩阵P*/
	16,  7, 20, 21,
	29, 12, 28, 17,
	 1, 15, 23, 26,
	 5, 18, 31, 10,
	 2,  8, 24, 14,
	32, 27,  3,  9,
	19, 13, 30,  6,
	22, 11,  4, 25	
};
char fp[] = {				/* final permutation F逆置换矩阵IP-1	  */
	40,  8, 48, 16, 56, 24, 64, 32,
	39,  7, 47, 15, 55, 23, 63, 31,
	38,  6, 46, 14, 54, 22, 62, 30,
	37,  5, 45, 13, 53, 21, 61, 29,
	36,  4, 44, 12, 52, 20, 60, 28,
	35,  3, 43, 11, 51, 19, 59, 27,
	34,  2, 42, 10, 50, 18, 58, 26,
	33,  1, 41,  9, 49, 17, 57, 25	
};
unsigned short randomize(int n) {	
	return (unsigned short)((rand())%n);
}
int writeKey(char *totalKey,int n) {
	char key[64] = {0};
	FILE *K;
	int i,j,k;
	int even = 0;
	if ((K = fopen("key.txt","wb"))==NULL) {
    	printf("can't open key.txt for clear\n");
    	return -1;
    }
    fclose(K);
    if ((K = fopen("key.txt","ab"))==NULL) {
    	printf("can't open key.txt for append\n");
    	return -2;
    }
    srand((int)time(0));
    for (j=0;j<n;j++) {
	    //for(i=0;i<64;i++) {    	
	    	//totalKey[j*64+i] = key[i] = (char)randomize(2);
	    //}
	    for(i=0;i<8;i++) {
	    	even = 0;//校验器清零 
	    	for (k=0;k<7;k++) {
	    		totalKey[j*64+i*8+k] = key[i*8+k] = (char)randomize(2);
	    		even++;
	    	}
	    	if (even%2==0) {//采用奇校验 
	    		totalKey[j*64+i*8+7] = key[i*8+7] = 1;
	    	}
	    }
	    fwrite(key,sizeof(char),64,K);
    }
    fclose(K);
    return 0;
}
int writeAuthKey(char *authName,int n) {
	char key[64] = {0};
	FILE *K;
	int i,j,k;
	int even = 0;
	char *totalKey = (char*)malloc(64*n*sizeof(char));
	if ((K = fopen(authName,"wb"))==NULL) {
    	printf("can't open %s for clear\n",authName);
    	return -1;
    }
    fclose(K);
    if ((K = fopen(authName,"ab"))==NULL) {
    	printf("can't open %s for append\n",authName);
    	return -2;
    }
    srand((int)time(0));
    for (j=0;j<n;j++) {
	    
	    for(i=0;i<8;i++) {
	    	even = 0;//校验器清零 
	    	for (k=0;k<7;k++) {
	    		totalKey[j*64+i*8+k] = key[i*8+k] = (char)randomize(2);
	    		even++;
	    	}
	    	if (even%2==0) {//采用奇校验 
	    		totalKey[j*64+i*8+7] = key[i*8+7] = 1;
	    	}
	    }
	    fwrite(key,sizeof(char),64,K);
    }
    fclose(K);
    return 0;
}
int readKey(keyElement **keys) {
	FILE *K;
	int i;
	int sum = 0;
	int num;
	char key[64] = {0};
	keyElement *k1,*k2;
	if ((K = fopen("key.txt","rb"))==NULL) {
		printf("can't open key.txt for read\n");
	}
	k1 = k2 = (keyElement *)malloc(sizeof(keyElement));
	while(num = fread(key,sizeof(char),64,K)) {
		sum++;		
		for (i=0;i<64;i++) {
			k1->data[i] = key[i];			
		}		
		if (sum==1) {
			*keys = k1;
		} else {
			k2->next = k1;
			k2 = k1;
		}
		k1 = (keyElement *)malloc(sizeof(keyElement));//重新申请
	}
	k2->next = NULL;
	k1 = NULL;
	k2 = NULL;
	return sum;
}
/*
* int2bin
*
* 将一个unsigned char类型的整数(0~255)转化为一个二进制数组
*
* @param unsigned char intValue 要转化的整数
* @param int *binArray 指向二进制数组的指针
* @param int n 要转化成的二进制数组的元素个数 
* @return void
*/
void int2bin(unsigned char intValue,unsigned char *binArray,int n) {
	int divValue;
	int modValue;
	int index = n-1;
	int i;
	for (i=0;i<=index;i++) {//清除原有数据 
		binArray[i] = 0;
	}
	//binArray = (char *)malloc(sizeof(char)*8);//函数内部不能申请初址
	//binArray[8] = {};
	divValue = intValue/2;
	modValue = intValue%2;
	binArray[index] = modValue;
	while(divValue!=0) {
		modValue = divValue%2;
		binArray[--index] = modValue;
		divValue/=2;
	}
	
}

int bin2int(char *bin,int binSize) {
	int i,j;
	int intValue = 0;
	
	for(i=binSize-1,j=0;i>=0;i--,j++) {
		intValue += (bin[i] * pow(2,j));	
	}
	return intValue;
}

void arrayCopy(char *dest,char *src,int n) {
	int i;
	for(i=0;i<n;i++) {
		dest[i] = src[i];
	}
}

void arrayMerge(char *sub1,char *sub2,char *mergeArray,int subCount) {//合并数组 
	int i;
	for(i=0;i<subCount;i++) {
		mergeArray[i] = sub1[i];
		mergeArray[i+subCount] = sub2[i];
	}
}
void arrayInvert(int *a,int n) {//反转数组 
	int half = n/2;
	int temp,i;
	
	for(i=0;i<half;i++) {
		temp = a[i];
		a[i] = a[n-1-i];
		a[n-1-i] = temp;
	}
	
}
/*
* get64Key
* 由8个整数转化为64位二进制，其中7位为数据位，一位为校验位
* 
* @param  char *int56Key 由8个整数构成的数组
* @param  char *charBinKey 要生成的64位二进制生成的数组
*
* @return void 
*/
void get64Key(char *int56Key,char *charBinKey) {
	int i,j;
	int oddNum = 0;
	
	
	for(i=0;i<8;i++) {
		int2bin(int56Key[i],charBinKey+8*i,7);
		for(j=8*i;j<8*i+7;j++) {//计算前7位奇数个数 
			if(charBinKey[j]==1) {
				oddNum++;
			}
		}
		/*
		if(oddNum%2==1) {//采用偶校验 
			charBinKey[8*i+7] = 1;
		}
		*/
		if(oddNum%2==0) {//采用奇数校验 
			charBinKey[8*i+7] = 1;
		}
		oddNum = 0;//计数器清零 
	}
}
void permute1(char *key64,char *key56) {
	char i;
	
	for(i=0;i<56;i++) {
		*(key56+i) = key64[pc1[i]-1]; 
	}
}
void permute2(char *key56,char *key48) {
	int i;
	
	for(i=0;i<48;i++) {
		key48[i] = key56[pc2[i]-1];
	}
}

void getInitSubKey(char *key56,char *leftKey,char *rightKey) {
	int i;
	for(i=0;i<28;i++) {
		leftKey[i] = key56[i];
		rightKey[i] = key56[i+28];
	}
}
void circulateOne(char *array,int arraySize) {
	char *temp = (char *)malloc(arraySize*sizeof(char));
	int i;
	*(temp+arraySize-1) = array[0];
	for(i=0;i<arraySize-1;i++) {
		*(temp+i) = array[i+1];
	}
	for(i=0;i<arraySize;i++) {
		*(array+i) = *(temp+i);
	}
	
	free(temp);
	temp = NULL;
}
void circulateTwo(char *array,char arraySize) {
	char *temp = (char *)malloc(arraySize*sizeof(int));
	int i;
	*(temp+arraySize-1) = array[1];
	*(temp+arraySize-2) = array[0];
	for(i=0;i<arraySize-2;i++) {
		*(temp+i) = array[i+2];
	}
	for(i=0;i<arraySize;i++) {
		*(array+i) = *(temp+i);
	}
	
	free(temp);
	temp = NULL;
}
void getSubKeys(char *initLeft,char *initRight,char subKeys[16][48]) {
	int i,j;
	char tempLeft[28] = {0};
	char tempRight[28] = {0};
	char tempKey[56] = {0};
	
	for(i=0;i<16;i++) {
		
		if(i==0 || i==1 || i==8 || i==15) {
			circulateOne(initLeft,28);
			circulateOne(initRight,28);
		} else {
			circulateTwo(initLeft,28);
			circulateTwo(initRight,28);
		}
		for(j=0;j<28;j++) {
			tempLeft[j] = initLeft[j];
			tempRight[j] = initRight[j];
			arrayMerge(tempLeft,tempRight,tempKey,28);
			permute2(tempKey,subKeys[i]);
		}
		
	}
}

int get64TextFromFile(char *binArray) {
	
	FILE *fd;
	int i;
	//unsigned char dataFromFile[8];//8*8=64位的数据,每个元素为一个字节
	unsigned char dataFromFile[] = {1,35,69,103,137,171,205,239}; 
	/*
	fd = fopen("a.txt","rb+");
	
	if(fd==NULL) {
		//printf("can't open %s!",*filename);
		return -1;
	}
	
		
	fread(dataFromFile,sizeof(char),8,fd);//读取8个字节
	*/
	
	for(i=0;i<8;i++) {
		int2bin(dataFromFile[i],binArray+i*8,8);	
	}
	
	return 0;	
}



void initPermuteByIP(char *binArray) {
	int i;
	char temp[64];
	
	for(i=0;i<64;i++) {
		temp[i] = binArray[ip[i]-1];	
	}
	
	for(i=0;i<64;i++) {
		binArray[i] = temp[i];
	}
}

void permuteByp32i(char *sArray) {//P置换 
	int i;
	char temp[32];
	
	for(i=0;i<32;i++) {
		temp[i] = sArray[p32i[i]-1];
	}
	for(i=0;i<32;i++) {
		sArray[i] = temp[i];
	}
}

void getNewRight(char *left,char *right,char *key) {
	int i,j;
	char tempNew48[48] = {0};//选择运算E产生的中间结果 
	char init48ToSbox[48] = {0};//S盒运算前产生的48位结果 
	int row,col;
	char rowBin[2] = {0};
	char colBin[4] = {0};
	char sTempInt;
	char sTempArray[32] = {0};//S盒运算产生的数组 
	
	for(i=0;i<48;i++) {
		tempNew48[i] = right[pe[i]-1];//选择E运算 
		
		init48ToSbox[i] = tempNew48[i] ^ key[i];
	}

	
	for(i=0;i<8;i++) {//S盒运算 ,分8组计算 
		
		rowBin[0] = init48ToSbox[i*6];///////##########
		colBin[0] = init48ToSbox[i*6+1];
		colBin[1] = init48ToSbox[i*6+2];
		colBin[2] = init48ToSbox[i*6+3];
		colBin[3] = init48ToSbox[i*6+4];
		rowBin[1] = init48ToSbox[i*6+5];
		
		row = bin2int(rowBin,2);
		col = bin2int(colBin,4);
		
		sTempInt = si[i][row*16+col];
		
		int2bin(sTempInt,sTempArray+i*4,4);
	
	}

	permuteByp32i(sTempArray);//32位置换 
	
	for(i=0;i<32;i++) {
		right[i] = left[i] ^ sTempArray[i];
	}	
}

void inversePermuteByFp(char *text) {
	int i;
	char tempText[64] = {0};
	
	for(i=0;i<64;i++) {
		tempText[i] = text[fp[i]-1];
	}
	for(i=0;i<64;i++) {
		text[i] = tempText[i];
	}
	
}
/**
* @param char *text 经过置换后的明文
* @param char subKeys[16][48] 16位子密钥 
**/
void code(char *text, char subKeys[16][48]) {
	char lText[32] = {0};
	char rText[32] = {0};
	char temp32Code[32] = {0};
	char temp48KeyL[48] = {0};
	int i,j;
	
	//printf("初始化右部的数据：\n");
	for(i=0;i<32;i++) {//初始化L0和R0 
		lText[i] = text[i];
		rText[i] = text[i+32];
		
	}
	for(i=1;i<=16;i++) {
		arrayCopy(temp32Code,rText,32);
		getNewRight(lText,rText,subKeys[i-1]);//得到新的rText 
		arrayCopy(lText,temp32Code,32);//得到新的lText  dest src len
		
	}
	arrayMerge(rText,lText,text,32);	
	
	inversePermuteByFp(text);//逆置换矩阵IP-1	
}
void init16Keys(char *initIntKey,char subKeys[16][48]) {
	char keyBin64[64] = {0};
	char keyBin56[56] = {0};//56位密钥
	char leftBinKey[28] = {0};//密钥置换过程的左边部分 
	char rightBinKey[28] = {0};//密钥置换过程中的右边部分 
	//char subKeys[16][48] = {0}; //生成的16个子密钥 
	get64Key(initIntKey,keyBin64);
	permute1(keyBin64,keyBin56);
	getInitSubKey(keyBin56,leftBinKey,rightBinKey);
	getSubKeys(leftBinKey,rightBinKey,subKeys);
}
void initMutilKeys(char *one64Key,char subKeys[16][48]) {
	char keyBin56[56] = {0};//56位密钥
	char leftBinKey[28] = {0};//密钥置换过程的左边部分 
	char rightBinKey[28] = {0};//密钥置换过程中的右边部分 	

	permute1(one64Key,keyBin56);//64位key产生56位key 
	getInitSubKey(keyBin56,leftBinKey,rightBinKey);
	getSubKeys(leftBinKey,rightBinKey,subKeys);
}
/**
* @param char *text64 64位要加密的二进制
* @param char subKeys[16][48] 16个子密钥 
**/
void desEncode(char *text64,char subKeys[16][48]) {
	int i;
	/*
	for (i=0;i<64;i++) {
		printf("%d ",text64[i]);
		if ((i+1)%8==0) {
			printf("\n");
		}
	}
	*/
	initPermuteByIP(text64);//初始置换IP 
	code(text64,subKeys);
}
/**
* @param char *text64 64位要解密的二进制
* @param char subKeys[16][48] 16个子密钥 
**/
void desDecode(char *text64,char subKeys[16][48]) {
	int i;
	char inverseSubKeys[16][48] = {0};
	
	for (i=0;i<16;i++) {//反转子密钥 
		arrayCopy(inverseSubKeys[i],subKeys[15-i],48);
	}
	initPermuteByIP(text64);//初始置换IP
	code(text64,inverseSubKeys); 
}

int encodeFile(char *soureFile,char *ecodedFile,char subKeys[16][48]) {
	FILE *fr,*fe;
	unsigned char dataFromFile[8] = {0};//8*8=64位的数据,每个元素为一个字节
	unsigned char dateEncode[8] = {0};//加密后的字节 
	unsigned char binArray[64] = {0};//64位二进制数据,作为明文
	int i,num=0;
	int temp = 0;
	//unsigned char encodeChar = 0;
	
	fr = fopen(soureFile,"rb");
	if(fr==NULL) {
		printf("can not open %s\n",soureFile);
		return -1;
	}
	fe = fopen(ecodedFile,"wb");
	if (fe==NULL) {
		printf("can not open %s to append\n",ecodedFile);
		return -2;
	}
	while((num = fread(dataFromFile,sizeof(char),8,fr))) {
		temp ++;
		
		if (num < 8) {//补足8个字节 
			for(i=num;i<8;i++) {
				dataFromFile[i] = 0;
			}
		}
		
		for (i=0;i<8;i++) {
			int2bin(dataFromFile[i],binArray+i*8,8);			
		}
		desEncode(binArray,subKeys);
		for (i=0;i<8;i++) {
			dateEncode[i] = bin2int(binArray+i*8,8);
		}
		fwrite(dateEncode,sizeof(char),8,fe);
	}
	fclose(fr);
	fclose(fe);	
	return temp;	
}
int decodeFile(char *ecodedFile,char *decodedFile,char subKeys[16][48]) {
	FILE *fe,*fd;

	unsigned char dataFromFile[8] = {0};//8*8=64位的数据,每个元素为一个字节
	unsigned char dateEncode[8] = {0};//加密后的字节 
	unsigned char binArray[64] = {0};//64位二进制数据,作为明文
	int i,num=0;
	int temp = 0;
	
	fe = fopen(ecodedFile,"rb");
	if (fe==NULL) {
		printf("can not open %s\n",ecodedFile);
		return -1;
	}
	fd = fopen(decodedFile,"wb");
	if (fd==NULL) {
		printf("can not open %s\n",decodedFile);
		return -2;
	}
	while(fread(dataFromFile,sizeof(char),8,fe)) {
		temp ++;
		for (i=0;i<8;i++) {
			int2bin(dataFromFile[i],binArray+i*8,8);			
		}
		desDecode(binArray,subKeys);
		for (i=0;i<8;i++) {
			dateEncode[i] = bin2int(binArray+i*8,8);
		}
		fwrite(dateEncode,sizeof(char),8,fd);
	}
	fclose(fe);
	fclose(fd);
	return temp;
}
int ecodeFileN(char *fileToEncode,char *fileToSave,int n) {
	FILE *fr,*fe;
	char *totalKey = (char *)malloc(sizeof(char)*64*n);
	char subKeys[16][48] = {0};
	int i;
	int sum = 0;
	if ((fr = fopen(fileToEncode,"rb"))==NULL) {
		printf("can't open %s for read\n",fileToEncode);
		return -1;
	}
	if ((fe = fopen(fileToSave,"wb"))==NULL) {
		printf("can't open %s for wirte\n",fileToSave);
		return -1;
	}	
	init16Keys(intKey,subKeys);
	encodeFile(fileToEncode,fileToSave,subKeys);
	writeKey(totalKey,n);
	for (i=0;i<n;i++) {
		initMutilKeys(totalKey+i*64,subKeys);
		encodeFile(fileToEncode,fileToSave,subKeys);
		sum++;
	}
	fclose(fr);
	fclose(fe);
	return sum;	
}
int decodeFileN(char *fileHasEncode,char *fileSaveDecode) {
	FILE *fe,*fd,*fk;
	char subKeys[16][48] = {0};
	keyElement *totalKey = NULL;
	int i;
	int sum = 0;
	
	if ((fe = fopen(fileHasEncode,"rb"))==NULL) {
		printf("can't open %s for read\n",fileHasEncode);
		return -1;
	}
	if ((fd = fopen(fileSaveDecode,"wb"))==NULL) {
		printf("can't open %s for save\n",fileSaveDecode);
		return -2;
	}
	if ((fk = fopen("key.txt","rb"))==NULL) {
		printf("can't open key.txt for read\n");
		return -3;
	}
	init16Keys(intKey,subKeys);
	decodeFile(fileHasEncode,fileSaveDecode,subKeys);
	readKey(&totalKey);
	while (totalKey!=NULL) {
		initMutilKeys(totalKey->data,subKeys);
		decodeFile(fileHasEncode,fileSaveDecode,subKeys);
		totalKey = totalKey->next;
		sum++;
	}
	fclose(fe);
	fclose(fd);
	fclose(fk);	
	return sum;
}
int changeRootKey() {
	int i;
	FILE *root;
	if ((root = fopen("root.txt","wb"))==NULL) {
		printf("can't open root.txt to save\n");
		return -1;
	}
	srand((int)time(0));
	
	for(i=0;i<8;i++) {
		intKey[i] = randomize(128);
		
		//printf("%d ",intKey[i]);
	}
	fwrite(intKey,sizeof(char),8,root);
	#define CHANGE_ROOT 1
	fclose(root);
	return 0;	
}
int getSessionKey(char *filename) {
	int i;
	char temp[8];
	FILE *sessionFile;
	if ((sessionFile = fopen(filename,"wb"))==NULL) {
		printf("can't open %s to save the session key\n",sessionFile);
		return -1;
	}
	srand((int)time(0));
	
	for(i=0;i<8;i++) {
		temp[i] = randomize(128);		
	}
	fwrite(temp,sizeof(char),8,sessionFile);
	fclose(sessionFile);
	return 0;
}
void getRootKey() {
	int i;
	FILE *root;
	root = fopen("root.txt","rb");
	#ifdef CHANGE_ROOT		
		#if (!root)			
			fread(intKey,sizeof(char),8,root);
		#endif
	#endif
	fclose(root);
}
int operation() 
{ 
	char *menu[]={
		"==== the operations you can select as follows ====\n", 
		" 1. encrypt a file with DES \n",
		" 2. encrypt a file with N-DES \n", 
		" 3. get the session key KS \n",
		" 4. get a authentication key \n", 
		" 5. change the root key \n"
		" 0. exit the program \n", 
		"==================================================\n"
	}; 
	char item[2]; // 以字符形式保存选择号 
	int c,i; // 定义整形变量 
	for(i=0;i<6;i++) { // 输出主菜单数组 
		cprintf("%s",menu[i]); 
	}
	do{ 
		printf("\nplease input 0~5 and press enter:");// 在菜单窗口外显示提示信息 
		scanf("%s",item); // 输入选择项 
		c=atoi(item); // 将输入的字符串转化为整形数 
	}while(c<0||c>15); // 选择项不在0~15之间重输 
	return c; // 返回功能选择 
} 
int main() {	
	int i,j;
	int result,selected,authKey;
		
	char subKeys[16][48] = {0};
	unsigned char binArray[64] = {0};//64位二进制数据,作为明文
	
	char fileToEncode[250] = "logo.gif";//要加密的文件名，做测试用，必须保证存在 
	char fileToSave[250] = "log.txt";
	char fileDecoded[250] = "logo2.gif";
	char sessionName[250] = "session";
	char authName[250] = "keya";
	for(;;) // 无限循环,选择0 退出循环 
	{ 
		switch(operation()) // 调用主菜单函数，按返回值选择功能函数 
		{ 
			
			case 1: //DES加密 
				printf("encode......\n");
				printf("please input the path of the file you want to encrypt(input 0 to cancle):");
				scanf("%s",fileToEncode);
				if (strcmp(fileToEncode,"0")!=0) {
					printf("please input the path of the file you want to save the encrypted file:");
					scanf("%s",fileToSave);
					getRootKey();
					init16Keys(intKey,subKeys);
					encodeFile(fileToEncode,fileToSave,subKeys);		
				} else {
					printf("you have cancled the process of encrypting a file\n");
				}
				
				printf("decode......\n");
				printf("please input the path of the file you want to decrypt(input 0 to cancel):");
				
				scanf("%s",fileToSave);
				if (strcmp(fileToSave,"0")!=0) {
					printf("please input the path of the file you want to save the decrypted file:");
					scanf("%s",fileDecoded);
					
					decodeFile(fileToSave,fileDecoded,subKeys);
					printf("end\n");
				} else {
					printf("you have cancled the process of deccrypting a file\n");
				}
			break;
			case 2: //N-DES加密 
				printf("encode......\n");
				printf("please input the path of the file you want to encrypt(input 0 to cancle):");
				scanf("%s",fileToEncode);
				if (strcmp(fileToEncode,"0")!=0) {
					printf("\nplease input the path of the file you want to save the encrypted file:");
					scanf("%s",fileToSave);
					printf("\nplease input the num of random key you want to use:");
					scanf("%d",&selected);
					getRootKey();
					ecodeFileN(fileToEncode,fileToSave,selected);		
				} else {
					printf("you have cancled the process of encrypting a file\n");
				}
				
				printf("decode......\n");
				printf("please input the path of the file you want to decrypt(input 0 to cancel):");
				
				scanf("%s",fileToSave);
				if (strcmp(fileToSave,"0")!=0) {
					printf("\nplease input the path of the file you want to save the decrypted file:");
					scanf("%s",fileDecoded);
					
					decodeFileN(fileToSave,fileDecoded);
					printf("end\n");
				} else {
					printf("you have cancled the process of deccrypting a file\n");
				}
			break; 
			case 3: //取得会话密钥 
				printf("you will get the session key, in the directory you program saved.\nplease input the name of session key:");
				scanf("%s",sessionName);
				getSessionKey(sessionName);
			break; 
			case 4: 
				printf("please input the name of the authentication key:");
				scanf("%s",authName);
				printf("please input the num of the authentication key:");
				scanf("%d",&authKey);
				writeAuthKey(authName,authKey);
			break; 			
			case 5: 
				printf("the defualt root key will be changed.\n");
				changeRootKey();
			break; 			
			case 0: exit(0); // 跳出循环，终止程序运行 
		} // switch语句结束 
	} // for循环结束 
}//end of main
