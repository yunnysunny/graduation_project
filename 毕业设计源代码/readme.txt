程序使用说明

==== the operations you can select as follows ====
 1. encrypt a file with DES
 2. encrypt a file with N-DES
 3. get the session key KS
 4. get a authentication key
 5. change the root key
 0. exit the program
上面是程序运行时输出的文本菜单，为了简化起见，将相同的部分全都删除掉，只保留第一次输出。
please input 0~5 and press enter:1
encode......
please input the path of the file you want to encrypt(input 0 to cancle):logo.gif
please input the path of the file you want to save the encrypted file:lo
decode......
please input the path of the file you want to decrypt(input 0 to cancel):lo
please input the path of the file you want to save the decrypted file:lo.gif
end
上面是DES加密的演示，选择加密的文件是程序所在文件夹下的logo.gif，加密后保存为lo，解密后保存为lo.gif
please input 0~5 and press enter:2
encode......
please input the path of the file you want to encrypt(input 0 to cancle):logo.gif
please input the path of the file you want to save the encrypted file:lo2
please input the num of random key you want to use:2
decode......
please input the path of the file you want to decrypt(input 0 to cancel):lo2
please input the path of the file you want to save the decrypted file:lo2.gif
end
上面是对N-DES加密的演示，操作之前会输出一个选择菜单，这里略过。操作步骤跟DES差不多，但是在加密的时候要输入随机密钥的数目，随机密钥数目越大越安全，当然运算速度也会相应减慢。
please input 0~5 and press enter:3
you will get the session key, in the directory you program saved.
please input the name of session key:session
接下来的两步实现类似PKI（公约基础设施）中RA（密钥注册中心）中的功能，上面这步操作会生成一个会话密钥，即5.2中提到的KS。
please input 0~5 and press enter:4
please input the name of the authentication key:keya
please input the num of the authentication key:5
“authentication key”，即5.2中提到的秘密密钥，用来鉴别通信双方的身份时使用。
please input 0~5 and press enter:5
the defualt root key will be changed.
N-DES中使用的主密钥是一般是固定的，但是为了安全性，也可以自己修改。上面的操作就是对主密钥的修改。修改完后，我又进行了一次DES加密，如下：
please input 0~5 and press enter:1
encode......
please input the path of the file you want to encrypt(input 0 to cancle):logo.gif
please input the path of the file you want to save the encrypted file:lg
decode......
please input the path of the file you want to decrypt(input 0 to cancel):lg
please input the path of the file you want to save the decrypted file:lg.gif
end
